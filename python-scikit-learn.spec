%global debug_package %{nil}

Name:           python-scikit-learn
Summary:        A Python module for machine learning built on top of SciPy
Version:        0.20.4
Release:        2
License:        BSD
URL:            https://scikit-learn.org/stable/
Source0:        https://github.com/scikit-learn/scikit-learn/archive/%{version}/scikit-learn-%{version}.tar.gz

%global _description\
scikit-learn is a Python module for machine learning built on top of SciPy\
and is distributed under the 3-Clause BSD license.\

%description %_description

%package -n python3-scikit-learn
Summary:        %summary

BuildRequires:  git python3-devel python3-numpy python3-Cython python3-pytest
Requires:       python3 >= 3.5 python3-numpy >= 1.11.0
Requires:       python3-scipy >= 0.17.0 python3-joblib >=  0.11

%description -n python3-scikit-learn %_description

%prep
%autosetup -n scikit-learn-%{version} -p1 -Sgit

%build
%py3_build

%install
%py3_install

%files -n python3-scikit-learn
%license COPYING
%doc examples/ CONTRIBUTING.md README.rst
%{python3_sitearch}/sklearn
%{python3_sitearch}/scikit_learn-*.egg-info

%changelog
* Wed Jan 22 2020 Jiangping Hu <hujp1985@foxmail.com> - 0.20.4-2
- Package init
